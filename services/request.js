const axios = require('axios');

module.exports = async (method, url, data) => {
  const options = {
    url, method,
    // headers: { 'content-type': 'application/x-www-form-urlencoded' },
    responseType: 'JSON'
  }

  method.toUpperCase() === 'POST' ? options.data = data : options.params = data

  console.log('options', options)
  const resultRequest = await axios(options)
  console.log('resultRequest', resultRequest)
  return resultRequest.data
}
