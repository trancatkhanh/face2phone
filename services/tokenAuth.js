const { config } = require('../config/config')

const jwt = require('jsonwebtoken');

const tokenAuth = {

    generateToken: function(payload) {
        return jwt.sign(payload, config.JWT_TOKEN_SECRET);
    },

    verifyToken: function(token, cb) {
        console.log('token', token);
        return jwt.verify(token, config.JWT_TOKEN_SECRET, {}, cb);
    },

    decodeToken: function(token, cb) {
        return jwt.decode(token, config.JWT_TOKEN_SECRET, {}, cb);
    }
};

module.exports = tokenAuth;
