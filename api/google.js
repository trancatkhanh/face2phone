const request = require('../services/request')
const { config } = require('../config/config')

const tokenAuth = require('../services/tokenAuth')

const google = {
    getUserInfo: async(code) => {
        const getAccessTokenUrl = `https://oauth2.googleapis.com/token`
        const getAccessTokenData = {
            code,
            client_id: config.GOOGLE_CLIENT_ID,
            client_secret: config.GOOGLE_CLIENT_SECRET,
            redirect_uri: config.GOOGLE_AUTH_CALLBACK_URL,
            grant_type: 'authorization_code'
        }
        console.log('getAccessTokenData', getAccessTokenData)
        let accessToken = {}
        try {
            accessToken = await request('POST', getAccessTokenUrl, getAccessTokenData)
            console.log('accessToken', accessToken)
        } catch (e) {
            console.log('xxxxxxx', e)
        }


        const getUserInfoUrl = `https://www.googleapis.com/oauth2/v1/userinfo`
        const getUserInfoData = {
            access_token: accessToken['access_token']
        }
        const userInfo = await request('GET', getUserInfoUrl, getUserInfoData)
        console.log('userInfo', userInfo)
        const token = await tokenAuth.generateToken(userInfo)

        console.log('token', token)
        if (userInfo) {
            return {
                status: 'success',
                data : { token }
            }
        }

        return {
            status: 'fail',
            data : {}
        }
    }
}

module.exports = google

// {
//   "code": "4/0AY0e-g7_Z6DCTTWnNOjxgK2wlAK2wzHRaXk6X3ycjpHTO2NXPrLE-VsaKF8qCztKNUQW8Q",
//     "client_id": "558605844750-fgeamfuba0aiucm9eakp3sr738lomm96.apps.googleusercontent.com",
//     "client_secret": "F4n9EVMxD5700Md-SFsg6uNT",
//     "redirect_uri": "http://localhost:1337/api/auth/google",
//     "grant_type": "authorization_code"
// }
