const request = require('../services/request')
const { config } = require('../config/config')

const facebook = {
    getPhone: async(uid) => {
        const url = `${config.VLTOOLKIT_API_BASEURL}/Convert?uid=${uid}&apikey=${config.VLTOOLKIT_API_KEY}`
        return await request('GET', url, {})
    }
}

module.exports = facebook
