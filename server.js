const express = require('express')
const cors = require('cors')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const { config } = require('./config/config')
const appRouter = require('./config/routes')

const app = express()
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(bodyParser.raw())
app.use(cookieParser());

// const originsWhitelist = [
//   'http://localhost:8000'
// ];
//
// const corsOptions = {
//   origin: (origin, callback) => {
//     var isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
//     callback(null, isWhitelisted);
//   },
//   credentials: true,
// };
//
// app.use(cors(corsOptions));

// Default path
app.get('/', (req, res) => {
  console.log('req', req.headers)
  if (req.headers.authorization === '123') {
    return res.status(401).send('false')
  }
  return res.status(200).send('true')
  // res.json({ message: 'dogrun-backend' });
});

app.use('/api', appRouter)

app.listen(config.SERVER_PORT, () => {
  console.log('Server is running on PORT', config.SERVER_PORT, config.NODE_ENV)
})

module.exports = app
