module.exports.config = {
  BASE_URL: process.env.BASE_URL || 'localhost',
  NODE_ENV: process.env.NODE_ENV || 'development',
  SERVER_PORT: process.env.PORT || 1337,

  VLTOOLKIT_API_KEY: '9C3A7C9B5A9C6B20827188837D8EEBA8',
  VLTOOLKIT_API_BASEURL: 'http://api.vltoolkit.com/api',

  GOOGLE_CLIENT_ID: '558605844750-q12te81erfp6jjvb1def6shncgdbuhuc.apps.googleusercontent.com',
  GOOGLE_CLIENT_SECRET: 'Mc4uqlrSqslo1FNiyLRisGZg',
  GOOGLE_AUTH_CALLBACK_URL: 'urn:ietf:wg:oauth:2.0:oob',

  JWT_TOKEN_SECRET: 'qwertyuiop1234567890)(*&^%$#@!'
}

// https://oauth2.googleapis.com/token
// https://www.googleapis.com/oauth2/v1/userinfo?access_token=ya29.a0AfH6SMB3lUrB5KrZ0k83OW5vcz3da7X2-J3Vae4x0tawkr-eQO9oMgO3eshCc_TFZ55ucYskE6itY9QJPEOE0ohl8jhEtlNsmOXBL45texopASOdrktOenoMG1nSBFfQPh0lNOQNWMGh2a6uMrfXt2gICBgj9gMGvDIoMHxjwv0

// {
//   "code": "4/0AY0e-g7_Z6DCTTWnNOjxgK2wlAK2wzHRaXk6X3ycjpHTO2NXPrLE-VsaKF8qCztKNUQW8Q",
//     "client_id": "558605844750-fgeamfuba0aiucm9eakp3sr738lomm96.apps.googleusercontent.com",
//     "client_secret": "F4n9EVMxD5700Md-SFsg6uNT",
//     "redirect_uri": "http://localhost:1337/api/auth/google",
//     "grant_type": "authorization_code"
// }
