const express = require('express');
const appRouter = express.Router();
const facebook = require('../api/facebook')
const google = require('../api/google')
const passport = require('passport');

const tokenAuth = require('../services/tokenAuth')

appRouter.get('/version', async (req, res) => {
    res.status(200).send('Hello, this is SPI version 1.0')
})

appRouter.get('/auth/google', async (req, res) => {
    console.log('req', req.query)
    const { code } = req.query
    const getUserInfo = await google.getUserInfo(code)
    res.status(200).send(getUserInfo)
})

appRouter.get('/scan/ext', async (req, res) => {
    console.log('req', req.query)
    const { token } = req.query
    res.status(200).send({
        status: 'success',
        data: []
    })
})

appRouter.get('/ext/version', async (req, res) => {
    res.status(200).send({
        status: 'success',
        data: '0.1'
    })
})

appRouter.get('/auth/me', async (req, res) => {
    console.log('req', req.query)
    const { token } = req.query
    const userInfo = await tokenAuth.decodeToken(token)
    console.log('userInfo', userInfo)
    res.status(200).send({
        status: 'success',
        data: {
            id: userInfo.id,
            email: userInfo.email,
            name: userInfo.name,
            availScans: 9999,
            totalScans: 0,
            picture: userInfo.picture,
            subscriptionPack: {
                expired: '',
                type: 'trial',
                name: 'demo'
            }
        }
    })
})

appRouter.post('/getPhone', async (req, res) => {
    console.log('req.body', req.body)
    let status = 'success'
    const { uid } = req.body
    const scanResult = await facebook.getPhone(uid)
    console.log('phonexxx', scanResult)

    if (scanResult['IsSuccess']) {
        return res.status(200).send({
            status,
            data: {
                uid, number: scanResult['Mobile']
            }
        })
    }

    return res.status(200).send({
        status: false,
        data: {}
    })
});

module.exports = appRouter
